---
codename: 'honami'
name: 'Sony Xperia Z1 (C610X)'
comment: 'community device'
icon: 'phone'
maturity: .25
noinstall: true
---
### Device specifications

|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8974 Snapdragon 800                                       |
|          CPU | Quad-core 2.2GHz Krait 400                                            | 
| Architecture | armv7                                                                 |
|          GPU | Adreno 330                                                            |
|      Display | 1080x1920                                                             |
|      Storage | 16 GB                                                                 |
|       Memory | 2 GB                                                                  |
|      Cameras | 20,7 MP, LED flash<br>2 MP, No flash                                  |
|   Dimensions | 144 mm (5.67 in) (h)<br>74 mm (2.91 in) (w)<br>8.5 (0.33 in) (d)      |

### Port status
|         Component | Status | Details                                       |
|------------------:|:------:|-----------------------------------------------|
|          AppArmor |    Y   |                                               |
|      Boot into UI |    Y   |                                               |
|            Camera |    N   | Requires gst-droid and even then barely works |
|    Cellular Calls |    Y   |                                               |
|     Cellular Data |    Y   |                                               |
|               GPS |    N   | Turns on but doesn't work                     |
|           Sensors |    N   | Doesn't use sensors.qcom                      |
|             Sound |    N   | Works with a manual switch only               |
| UBPorts Installer |    N   |                                               |
|  UBPorts Recovery |    N   |                                               |
|          Vibrator |    Y   |                                               |
|             Wi-Fi |    Y   |                                               |
        
### Maintainer(s)
konradybcio


### Source repos
https://github.com/rhine-dev

### CI builds
https://github.com/rhine-dev/ubports-ci
