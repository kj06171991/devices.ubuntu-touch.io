---
codename: 'oneplus2'
name: 'Oneplus 2'
comment: 'wip'
icon: 'phone'
image: 'https://wiki.lineageos.org/images/devices/oneplus2.png'
maturity: .6
---

### Device specifications

|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8994 Snapdragon 810                                       |
|          CPU | Quad-core 1.8 GHz Cortex A57 + quad-core 1.5 GHz Cortex A53           |
| Architecture | arm64                                                                 |
|          GPU | Adreno 430                                                            |
|      Display | 5.5 in 1920 x 1080                                                    |
|      Storage | 16/64 GB                                                              |
|       Memory | 3/4 GB                                                                |
| Rear Camera  | 13 MP, dual-LED flash                                                 |
| Front Camera | 5 MP                                                                  |
| Release Date | July 2015                                                             |
|   Dimensions | 151.8 x 74.9 x 9.85 mm                                                |

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|        Bluetooth  |    N   |                    |
|            Camera |    ?   |      Untested      |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    Y   |                    |
|       Fingerprint |    N   |                    |
|               GPS |    Y   |                    |
|           Sensors |    ?   |      Untested      |
|             Sound |    Y   |                    |
| UBPorts Installer |    N   |      WIP           |
|  UBPorts Recovery |    Y   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |


### Maintainer(s)
Vince1171

### Forum topic
[Oneplus 2](https://forums.ubports.com/topic/4253/oneplus-2)

### Source repos
[Device Tree](https://github.com/Halium/android_device_oneplus_oneplus2)
[Kernel](https://github.com/Halium/android_kernel_oneplus_msm8994)

