---
codename: 'santoni'
name: 'Xiaomi Redmi 4(X)'
comment: 'wip'
icon: 'phone'
image: 'https://wiki.lineageos.org/images/devices/santoni.png'
noinstall: true
maturity: .3
---

### Device specifications

|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8940 Snapdragon 435                                       |
|          CPU | Octa-core 1.4 GHz Cortex-A53                                          |
| Architecture | arm64                                                                 |
|          GPU | Adreno 505                                                            |
|      Display | 720x1280                                                              |
|      Storage | 16 GB / 32 GB / 64GB                                                  |
|       Memory | 2 GB / 3 GB / 4 GB                                                    |
|      Cameras | 13 MP, LED flash<br>5 MP, No flash                                    |
|   Dimensions | 139.2 mm (5.48 in) (h)<br>70 mm (2.76 in) (w)<br>8.7 mm (0.34 in) (d) |

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|            Camera |    Y   | Requires gst-droid |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    Y   |                    |
|               GPS |    Y   |                    |
|           Sensors |    Y   |                    |
|             Sound |    Y   |                    |
| UBPorts Installer |    N   |                    |
|  UBPorts Recovery |    Y   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |

### Maintainer(s)
Danct12

### Forum topic
https://forums.ubports.com/topic/3682/xiaomi-redmi-4x-santoni

### Source repos
https://github.com/ubports-santoni

### CI builds
https://github.com/ubports-santoni/ubports-ci
