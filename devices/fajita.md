
---
codename: 'fajita'
name: 'OnePlus 6T'
comment: 'community device'
icon: 'phone'
image: 'https://wiki.lineageos.org/images/devices/fajita.png'
maturity: .8
---

### Device specifications

|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
| SoC          | Qualcomm SDM845 Snapdragon 845                                        |
| RAM          | 6/8 GB LPDDR4X                                                        |
| CPU          | Octa-core Kryo 385 4 x 2.8 GHz + 4 x 1.7 GHs arm64                    |
| GPU          | Adreno 630                                                            |
| Storage      | 128/256 GB UFS2.1                                                     |
| Camera       | 16 MP                                                                 |
| Battery      | Non-removable Li-Po 3700 mAh 

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|            Camera |    Y   |                    |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    Y   |                    |
|               GPS |    Y   |                    |
|           Sensors |    Y   |                    |
|             Sound |    Y   |                    |
| UBPorts Installer |    N   |                    |
|  UBPorts Recovery |    N   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |

[More frequently updated version of table above](https://github.com/ubports-oneplus6/documentation/blob/master/progress.md)

### Maintainer(s)
MrCyjaneK

### Forum topic
https://forums.ubports.com/topic/4578/oneplus-6-t-port

### Source repos
https://github.com/ubports-oneplus6/
