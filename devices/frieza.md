---
codename: 'frieza'
name: 'Bq Aquaris M10 FHD'
comment: ''
icon: 'tablet-landscape'
image: 'https://ubports.com/web/image/1523/devices-bq-aquaris-main.png'
maturity: .999
---

## The tablet to travel with

The Aquaris is a tablet that is both thin and light with a screen of 10''. It works smoothly with a recognizable Ubuntu interface. This tablet provides you a great workflow as a portable laptop. Imagine travelling by plane and working on your documents using LibreOffice.

**Note**: Bq m10 FHD devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturers Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
